package com.crosschx.touch.atmosphere;

import org.atmosphere.config.service.Get;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Singleton;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceEventListenerAdapter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * Handles the websocket communication
 */
@ManagedService(path = "/touch", broadcaster = org.atmosphere.util.SimpleBroadcaster.class)
@Singleton
public class TouchRequestHandler {

    private final Logger logger = Logger.getAnonymousLogger();
    private final TouchReader reader = new TouchReader();

    @Get
    public void onOpen(final AtmosphereResource resource) throws IOException {
        Util.logMessage(logger, Level.INFO, "Client onOpen", null);
        resource.addEventListener(new AtmosphereResourceEventListenerAdapter() {
            @Override
            public void onSuspend(AtmosphereResourceEvent event){
           Util.logMessage(logger, Level.INFO, "Client onOpen onSuspend", null);
                reader.putResource(event.getResource());
            }

            @Override
            public void onResume(AtmosphereResourceEvent event){
            Util.logMessage(logger, Level.INFO,"Client onOpen onResume", null);
                reader.putResource(event.getResource());
            }

            @Override
            public void onDisconnect(AtmosphereResourceEvent event) {
            logger.info("Client onOpen websocketChat");
                reader.removeResoure(event.getResource());
                if (event.isCancelled()) {
                    Util.logMessage(logger, Level.INFO, "Browser {} unexpectedly disconnected:" + event.getResource().uuid(), null);
                } else if (event.isClosedByClient()) {
                    Util.logMessage(logger, Level.INFO, "Browser {} closed the connection" + event.getResource().uuid(), null);
                }
            }
        });
    }
}