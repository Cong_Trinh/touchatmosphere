package com.crosschx.touch.atmosphere;

import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.nettosphere.Config;
import org.atmosphere.nettosphere.Nettosphere;

/**
 * Created by congtrinh on 8/6/15.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Config.Builder builder = new Config.Builder();
        builder.resource(TouchRequestHandler.class)
                //Uncomment for chat web ui
                // For *-distrubution
                //.resource("./webapps")
                // For running inside an IDE
                //.resource("/home/ubuntu/Dev/TouchAtmosphere/src/com/crosschx/webapp")
                .port(8040)
                .initParam(ApplicationConfig.ANALYTICS, "false")
                .build();
        Nettosphere server = new Nettosphere.Builder().config(builder.build()).build();
        server.start();
    }
}
