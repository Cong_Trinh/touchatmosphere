package com.crosschx.touch.atmosphere;

import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.readers.DPFPReadersCollection;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A library of utility functions
 *
 * @author  Ian Gall
 */
class Util {

	/**
	 * Retrieves the internal serial number of the first attached fingerprint
	 * reader found
	 *
	 * @return  the internal serial number
	 */
	public static String getInternalSerialNumber() {
		String internalSerial = "";
		// Get all readers
		DPFPReadersCollection readersCollection = DPFPGlobal.getReadersFactory().getReaders();
		// Get the first reader's serial number, if available
		if (readersCollection != null && readersCollection.size() > 0) {
			internalSerial = readersCollection.get(0).getSerialNumber();
		}
		return internalSerial.trim();
	}

	/**
	 * Retrieves the MAC Address of the computer
	 *
	 * @return            the MAC Address
	 * @throws Exception  if the host can't be resolved
	 */
	public static String getMacAddress() throws Exception {
		String macAddress = "";
		try {
			macAddress = getMacAddressPrimary();
		} catch (Exception ignored) {
			macAddress = getMacAddressFallback();
		}
		return macAddress.trim().toLowerCase();
	}

	/**
	 * Retrieves the MAC Address of the computer by IP address
	 *
	 * @return            the MAC Address
	 * @throws Exception  if the host can't be resolved
	 */
	private static String getMacAddressPrimary() throws Exception {
		String macAddress = "";
		InetAddress inetAddress = InetAddress.getLocalHost();
		NetworkInterface networkInterface = NetworkInterface.getByInetAddress(inetAddress);
		macAddress = translateMacAddressBytes(networkInterface.getHardwareAddress());
		return macAddress;
	}

	/**
	 * Retrieves the MAC Address of the computer by getting the first network interface's address
	 *
	 * @return            the MAC Address
	 * @throws Exception  if the host can't be resolved
	 */
	private static String getMacAddressFallback() throws Exception {
		String macAddress = "";
		Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
		while (networkInterfaces.hasMoreElements()) {
			NetworkInterface networkInterface = networkInterfaces.nextElement();
			if (networkInterface != null) {
				macAddress = translateMacAddressBytes(networkInterface.getHardwareAddress());
				if (!macAddress.equals("")) {
					break;
				}
			}
		}
		return macAddress;
	}

	/**
	 * Translates a MAC Address of byte[] to a hex string version
	 *
	 * @param macAddressBytes  the MAC address byte[]
	 * @return                 the translated MAC Address
	 */
	private static String translateMacAddressBytes(byte[] macAddressBytes) {
		return DatatypeConverter.printHexBinary(macAddressBytes);
	}

	/**
	 * Retrieves the hostname of the computer
	 *
	 * @return            the hostname
	 * @throws Exception  if the host can't be resolved
	 */
	public static String getHostname() throws Exception {
		String hostname = "";
		BufferedReader reader = null;
		try {
			Process process = Runtime.getRuntime().exec("hostname");
			process.waitFor();
			reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			hostname = reader.readLine();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception ignored) {}
			}
		}
		return hostname.trim();
	}

	/**
	 * Retrieves the application.properties key/values
	 *
	 * @return  the collection of properties
	 */
	public static InputStream getAppProperties() {
		return Util.class.getClassLoader().getResourceAsStream("META-INF/application.properties");
	}

	/**
	 * Logs a message, using a predefined structure, optionally with exception details
	 *
	 * @param logger   the logger object
	 * @param level    the logger level to target
	 * @param message  the main message to log
	 * @param ex       the exception
	 */
	public static void logMessage(Logger logger, Level level, String message, Throwable ex) {
		String finalMsg = "##CrossChx Fingerprint Service## " + message;
		if (ex != null) {
			finalMsg += ": " + ex.toString();
			finalMsg += "\n" + getExceptionStackTrace(ex);
		}
		logger.log(level, finalMsg);
	}

	/**
	 * Converts a Throwable's stacktrace to String
	 *
	 * @param ex  the exception
	 * @return    the stack trace
	 */
	public static String getExceptionStackTrace(Throwable ex) {
		StringWriter writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		ex.printStackTrace(printWriter);
		printWriter.flush();
		printWriter.close();
		return writer.toString();
	}
}
