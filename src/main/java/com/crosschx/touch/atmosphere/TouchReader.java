package com.crosschx.touch.atmosphere;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.DPFPCapturePriority;
import com.digitalpersona.onetouch.capture.event.*;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.atmosphere.cpr.AtmosphereResource;
import org.eclipse.jetty.util.StringUtil;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Applet class for handling Digital Persona-compliant fingerprint reader events.
 * Using the applet, these following events can be accessed:
 *    onInit
 *    onConnect
 *    onDisconnect
 *    onStartCapture
 *    onStopCapture
 *    onDestroyed
 *    onFingerDown
 *    onImageAcquired
 *    onFeaturesAcquired
 *    onFingerUp
 *    onGeneralError
 *    onLog
 *
 * @author  Ian Gall
 * @author  Shaunn Barron
 * @author  Cong Trinh
 */
public class TouchReader {

	private static final Logger logger = Logger.getAnonymousLogger();
	private Properties myProperties = new Properties();
	private boolean isRteInstalled = true;
	private String macAddress = "";
	private String hostname = "";
	private static DPFPCapture capturer;
	private Map<String, AtmosphereResource> clientList = new HashMap<String, AtmosphereResource>();

	public TouchReader(){
		Util.logMessage(logger, Level.INFO, "Starting TouchReader", null);
        initAll();
        startCapture();
    }

    public void putResource(AtmosphereResource resource){
    	clientList.put(resource.uuid(), resource);
		if(StringUtil.isNotBlank(getLocalInternalSerial())){
			fireJavascriptEvent("startCapture", true);
			macAddress = getLocalMacAddress();
			hostname = getLocalHostname();
			String internalSerial = getLocalInternalSerial();
			fireJavascriptEvent("connect", internalSerial, macAddress, hostname);
		}
    }

    public void removeResoure(AtmosphereResource resource){
    	clientList.remove(resource.uuid());
    }

	/**
	 * Initializes all things needed
	 */
	private void initAll() {
		initMembers();
		initCapturer();

		String myVersion = myProperties.getProperty("app.version");
		if (myVersion != null) {
			Util.logMessage(logger, Level.INFO, "Version - " + myVersion, null);
			fireJavascriptEvent("log", "Version - " + myVersion);
		}

		fireJavascriptEvent("init", isRteInstalled, macAddress, hostname);
	}

	/**
	 * Attempts to start the DigitalPersona SDK's capturing ability
	 */
	private void startCapture() {
	Util.logMessage(logger, Level.SEVERE, "capturer.isStarted" + String.valueOf(capturer.isStarted()), null);
		boolean success = false;
		try {
			capturer.startCapture();
			Util.logMessage(logger, Level.SEVERE, "capturer.isStarted" + String.valueOf(capturer.isStarted()), null);
			success = true;
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem starting capture", ex);
			if (isRteInstalled) {
				String desc = "Problem starting capture",
				       reason = ex.toString();
				fireJavascriptEvent("generalError", desc, reason);
			}
		}
		fireJavascriptEvent("startCapture", success);
	}


	/**
	 * Initializes all private members
	 */
	private void initMembers() {
		macAddress = getLocalMacAddress();
		hostname = getLocalHostname();
        myProperties = getAppProperties();
	}

	/**
	 * Initializes the capturer and its listeners
	 */
	private void initCapturer() {
		capturer = DPFPGlobal.getCaptureFactory().createCapture();
		capturer.setPriority(DPFPCapturePriority.CAPTURE_PRIORITY_NORMAL);
		// Initialize the extractor (can fail if the RTE isn't installed)
		DPFPFeatureExtraction extractor;
		try {
			extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
		} catch (UnsatisfiedLinkError ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem creating extractor", ex);
			isRteInstalled = false;
			String desc = "Problem creating extractor",
			       reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
			return;
		}
		// Initialize the reader listeners
		initReaderStatusListener();
		initDataListener(extractor);
		initSensorListener();
	}

	/**
	 * Initializes the reader status listener for connections and disconnections
	 */
	private void initReaderStatusListener() {
        capturer.addReaderStatusListener(new DPFPReaderStatusListener() {
			@Override
			public void readerConnected(DPFPReaderStatusEvent dpfprs) {
				macAddress = getLocalMacAddress();
				hostname = getLocalHostname();
				String internalSerial = getLocalInternalSerial();
				fireJavascriptEvent("connect", internalSerial, macAddress, hostname);
			}

			@Override
			public void readerDisconnected(DPFPReaderStatusEvent dpfprs) {
				fireJavascriptEvent("disconnect");
			}
		});
	}

	/**
	 * Initializes the data listener for acquired features
	 *
	 * @param extractor  the feature extractor
	 */
	private void initDataListener(final DPFPFeatureExtraction extractor) {
		capturer.addDataListener(new DPFPDataListener() {
            @Override
            public void dataAcquired(DPFPDataEvent dpfpde) {
                DPFPSample sample = dpfpde.getSample();
                boolean goodPrint = true;
                DPFPFeatureSet enrollFeatureSet = null;
                DPFPFeatureSet verifyFeatureSet = null;
                String sendingEnrollFeatureSet = "",
                        sendingVerifyFeatureSet = "",
                        encodedImage = "";

                // Extract feature sets from the sample
                enrollFeatureSet = extractEnrollFeatureSet(extractor, sample);
                if (enrollFeatureSet == null) {
                    goodPrint = false;
                }
                verifyFeatureSet = extractVerifyFeatureSet(extractor, sample);
                if (verifyFeatureSet == null) {
                    goodPrint = false;
                }

                if (goodPrint) {
                    // Convert feature sets into transmittable data
                    sendingEnrollFeatureSet = DatatypeConverter.printHexBinary(enrollFeatureSet.serialize());
                    sendingVerifyFeatureSet = DatatypeConverter.printHexBinary(verifyFeatureSet.serialize());
                    // Convert captured image into transmittable data
                    encodedImage = extractEncodedFeatureImage(sample);
                }

                fireJavascriptEvent("featuresAcquired", goodPrint, sendingEnrollFeatureSet, sendingVerifyFeatureSet, encodedImage);
            }
        });
	}

	/**
	 * Extracts the enroll feature set from a sample
	 *
	 * @param extractor  the feature extractor
	 * @param sample     the feature sample
	 * @return           the enroll feature set
	 */
	private DPFPFeatureSet extractEnrollFeatureSet(DPFPFeatureExtraction extractor, DPFPSample sample) {
		DPFPFeatureSet enrollFeatureSet = null;
		try {
			enrollFeatureSet = extractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem extracting enroll feature set", ex);
			String desc = "Unable to extract enroll feature set",
			       reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
		}
		return enrollFeatureSet;
	}

	/**
	 * Extracts the verify feature set from a sample
	 *
	 * @param extractor  the feature extractor
	 * @param sample     the feature sample
	 * @return           the verify feature set
	 */
	private DPFPFeatureSet extractVerifyFeatureSet(DPFPFeatureExtraction extractor, DPFPSample sample) {
		DPFPFeatureSet verifyFeatureSet = null;
		try {
			verifyFeatureSet = extractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem extracting verify feature set", ex);
			String desc = "Unable to extract verify feature set",
			       reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
		}
		return verifyFeatureSet;
	}

	/**
	 * Extracts an image from a sample and Base64 encodes it
	 *
	 * @param sample  the feature sample
	 * @return        the encoded image
	 */
	private String extractEncodedFeatureImage(DPFPSample sample) {
		String featureImage = "";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			BufferedImage image = (BufferedImage) DPFPGlobal.getSampleConversionFactory().createImage(sample);
			ImageIO.write(image, "png", baos);
			featureImage = DatatypeConverter.printBase64Binary(baos.toByteArray());
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem extracting fingerprint image", ex);
			String desc = "Unable to extract fingerprint image",
			       reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
		} finally {
			try {
				baos.close();
			} catch (Exception ignored) {}
		}
		return featureImage;
	}

	/**
	 * Initializes the sensor listener for finger events
	 */
	private void initSensorListener() {
		capturer.addSensorListener(new DPFPSensorListener() {
			@Override
			public void fingerTouched(DPFPSensorEvent dpfpse) {
				fireJavascriptEvent("fingerDown");
			}

			@Override
			public void fingerGone(DPFPSensorEvent dpfpse) {
				fireJavascriptEvent("fingerUp");
			}

			@Override
			public void imageAcquired(DPFPSensorEvent dpfpse) {
				fireJavascriptEvent("imageAcquired");
			}
		});
	}

	/**
	 * Attempts to call an applet-parameter-specified JavaScript event
	 *
	 * @param eventName  the event name
	 * @param params     list of arguments to trigger the event with
	 */
	private void fireJavascriptEvent(String eventName, Object... params) {
		// Lookup JS function to execute for this event type
        try {
            broadcastEvent(eventName, params);
		} catch (Exception ex) {
			if (eventName.equals("generalError")) {
				Util.logMessage(logger, Level.SEVERE, "Problem communicating with JavaScript.", ex);
			} else {
				String desc = "Unable to call " + eventName + " event",
				       reason = ex.toString(),
				       stacktrace = Util.getExceptionStackTrace(ex);
				fireJavascriptEvent("generalError", desc, reason, stacktrace);
			}
		}
	}

	/**
	 * Broadcast event for method with optional parameters
	 *
	 * @param function  the function name
	 * @param params    the parameters to call the script with
	 */
	private void broadcastEvent(String function, Object... params) {
		Util.logMessage(logger, Level.INFO, function, null);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object > asMap = new HashMap<String, Object>();
		asMap.put("readerEvent", function);
		asMap.put("message", params);
		try {
			for(String client : clientList.keySet()) {
				clientList.get(client).getBroadcaster().broadcast(mapper.writeValueAsString(asMap));
			}
		} catch (Exception ex){
			Util.logMessage(logger, Level.SEVERE, "Problem broadcasting event", ex);
		}
	}

	/**
	 * Safely retrieves the internal serial number
	 *
	 * @return  the serial number
	 */
	private String getLocalInternalSerial() {
		String internalSerial = "";
		if (isRteInstalled) {
			try {
				internalSerial = Util.getInternalSerialNumber();
			} catch (Exception ex) {
				Util.logMessage(logger, Level.SEVERE, "Problem retrieving internal serial", ex);
				String desc = "Unable to retrieve internal serial",
				       reason = ex.toString();
				fireJavascriptEvent("generalError", desc, reason);
			}
		}
		return internalSerial;
	}

	/**
	 * Safely retrieves the MAC address of the computer
	 *
	 * @return  the MAC address
	 */
	private String getLocalMacAddress() {
		String localMac = "";
		try {
			localMac = Util.getMacAddress();
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem retrieving mac address", ex);
			String desc = "Unable to retrieve mac address",
			       reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
		}
		return localMac;
	}

	/**
	 * Safely retrieves the hostname of the computer
	 *
	 * @return  the hostname
	 */
	private String getLocalHostname() {
		String localHost = "";
		try {
			localHost = Util.getHostname();
		} catch (Exception ex) {
			Util.logMessage(logger, Level.SEVERE, "Problem retrieving hostname", ex);
			String desc = "Unable to retrieve hostname",
					reason = ex.toString();
			fireJavascriptEvent("generalError", desc, reason);
		}
		return localHost;
	}

    /**
     * Safely retrieves the application.properties key/values
     *
     * @return  the application's properties
     */
    private Properties getAppProperties() {
        Properties props = new Properties();
        try {
            props.load(Util.getAppProperties());
        } catch (Exception ex) {
            Util.logMessage(logger, Level.SEVERE, "Problem retrieving app properties", ex);
            String desc = "Unable to retrieve app properties",
                    reason = ex.toString();
            fireJavascriptEvent("generalError", desc, reason);
        }
        return props;
    }
}
