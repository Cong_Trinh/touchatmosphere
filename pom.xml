<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.safechx</groupId>
    <artifactId>safechx.touch.atmosphere</artifactId>
    <version>1.0</version>

    <properties>
        <java-version>1.6</java-version>
        <maven.compiler.target>${java-version}</maven.compiler.target>
        <maven.compiler.source>${java-version}</maven.compiler.source>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${project.build.sourceEncoding}</project.reporting.outputEncoding>

        <digitalpersona-onetouch-version>1.6.1</digitalpersona-onetouch-version>
        <sun-plugin-version>1.6</sun-plugin-version>
        <jett-server>9.2.11.v20150529</jett-server>
        <atmosphere-client>2.2.12</atmosphere-client>
        <atmosphere-runtime>2.3.3</atmosphere-runtime>
        <atmosphere-netto>2.3.3</atmosphere-netto>
        <geronimo-servlet>1.0</geronimo-servlet>
        <jackson-databind>2.6.0</jackson-databind>
        <logback>1.1.3</logback>

        <maven-lifecycle-mapping-version>1.0.0</maven-lifecycle-mapping-version>
        <maven-surefire-plugin-version>2.17</maven-surefire-plugin-version>
        <maven-properties-plugin-version>1.0-alpha-2</maven-properties-plugin-version>
        <maven-shade-plugin-version>2.3</maven-shade-plugin-version>
        <maven-jarsigner-plugin-version>1.3.2</maven-jarsigner-plugin-version>
        <maven-javadoc-plugin-version>2.9.1</maven-javadoc-plugin-version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>${jett-server}</version>
        </dependency>

        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-servlet</artifactId>
            <version>${jett-server}</version>
        </dependency>

        <dependency>
            <groupId>org.eclipse.jetty.websocket</groupId>
            <artifactId>websocket-server</artifactId>
            <version>${jett-server}</version>
        </dependency>

        <dependency>
            <groupId>org.atmosphere.client</groupId>
            <artifactId>jquery</artifactId>
            <version>${atmosphere-client}</version>
            <type>war</type>
        </dependency>

        <dependency>
            <groupId>org.atmosphere</groupId>
            <artifactId>atmosphere-runtime</artifactId>
            <version>${atmosphere-runtime}</version>
        </dependency>

        <dependency>
            <groupId>org.atmosphere</groupId>
            <artifactId>nettosphere</artifactId>
            <version>${atmosphere-netto}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.geronimo.specs</groupId>
            <artifactId>geronimo-servlet_3.0_spec</artifactId>
            <version>${geronimo-servlet}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson-databind}</version>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback}</version>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>${logback}</version>
        </dependency>

        <dependency>
            <groupId>com.digitalpersona.onetouch</groupId>
            <artifactId>dpotapi</artifactId>
            <version>${digitalpersona-onetouch-version}</version>
        </dependency>
        <dependency>
            <groupId>com.digitalpersona.onetouch</groupId>
            <artifactId>dpotjni</artifactId>
            <version>${digitalpersona-onetouch-version}</version>
        </dependency>
    </dependencies>

    <build>
        <!-- Filter the application properties file -->
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>application.properties</include>
                    <include>assembly.properties</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <!-- Configure tests -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven-surefire-plugin-version}</version>
                <configuration>
                    <skipTests>true</skipTests>
                    <systemPropertyVariables>
            <app.version>propertyValue</app.version>
          </systemPropertyVariables>
                </configuration>
            </plugin>

            <!-- Load properties from external files -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <version>${maven-properties-plugin-version}</version>
                <executions>
                    <execution>
                        <phase>initialize</phase>
                        <goals>
                            <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                            <files>
                                <file>${basedir}/src/main/resources/assembly.properties</file>
                                <file>${basedir}/src/main/resources/codesign.properties</file>
                                <file>${basedir}/src/main/resources/application.properties</file>
                            </files>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Combine all dependencies into one JAR -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>${maven-shade-plugin-version}</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <filters>
                        <filter>
                            <artifact>*:*</artifact>
                            <excludes>
                                <exclude>META-INF/maven/**</exclude>
                            </excludes>
                        </filter>
                    </filters>
                    <transformers>
                        <transformer
                                implementation="org.apache.maven.plugins.shade.resource.IncludeResourceTransformer">
                            <resource>META-INF/application.properties</resource>
                            <file>${project.build.outputDirectory}/application.properties</file>
                        </transformer>
                        <transformer
                                implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                            <manifestEntries>
                                <Codebase>${assembly.Domains}</Codebase>
                                <Permissions>${assembly.Permissions}</Permissions>
                                <Application-Library-Allowable-Codebase>${assembly.Domains}
                                </Application-Library-Allowable-Codebase>
                                <Caller-Allowable-Codebase>${assembly.Domains}</Caller-Allowable-Codebase>
                                <Application-Name>${assembly.ApplicationName}</Application-Name>
                                <Implementation-Title>${assembly.ImplementationTitle}</Implementation-Title>
                                <Implementation-Version>${assembly.ImplementationVersion}</Implementation-Version>
                                <Implementation-Vendor-Id>${assembly.ImplementationVendorId}</Implementation-Vendor-Id>
                            </manifestEntries>
                        </transformer>
                        <transformer
                                implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                            <manifestEntries>
                                <Main-Class>com.crosschx.touch.atmosphere.Main</Main-Class>
                                <Build-Number>1</Build-Number>
                            </manifestEntries>
                        </transformer>
                    </transformers>
                </configuration>
            </plugin>

            <!-- Sign the code with a code signing certificate -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jarsigner-plugin</artifactId>
                <version>${maven-jarsigner-plugin-version}</version>
                <executions>
                    <execution>
                        <id>sign</id>
                        <phase>package</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>verify</id>
                        <phase>package</phase>
                        <goals>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <keystore>${basedir}/keystore/codesignstore</keystore>
                    <alias>${codesign.alias}</alias>
                    <storepass>${codesign.storepass}</storepass>
                    <keypass>${codesign.keypass}</keypass>
                    <verbose>false</verbose>
                    <certs>true</certs>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>